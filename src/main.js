import Vue from 'vue';
import VueRouter from 'vue-router';
import VueNativeSock from 'vue-native-websocket'

import routes from './routes';
import App from './App.vue';

Vue.config.productionTip = false
Vue.use(VueRouter);
console.log(process.env);
Vue.use(
  VueNativeSock, 
  `ws://${process.env.VUE_APP_LUMINE_ADDRESS}:${process.env.VUE_APP_LUMINE_PORT}/ws`, 
  {
    format: 'json', 
    debug:true,
    reconnection: true,
    reconnectionDelay: 3000,
  }
)

const router = new VueRouter({mode: 'history', routes});

new Vue({
  router,
  // store,
  render: h => h(App),
}).$mount('#app');