import ListLogs from './components/ListLogs.vue';
import StaticResponses from './components/StaticResponses.vue';
import StaticResponsesForm from './components/StaticResponsesForm.vue';

const routes = [
    { path: '/', component: ListLogs },
    { path: '/responses', component: StaticResponses },
    { path: '/responses/new', component: StaticResponsesForm, name: "CreateStaticResponse" },
    { path: '/response/:id', component: StaticResponsesForm, name: "UpdateStaticResponse" },
];


export default routes;